import React from 'react';
import './App.css';
import 'normalize.css';

import Tile from './components/Tile';

function App() {
  return (
    <div className="App">
      <Tile />
    </div>
  );
}

export default App;
